#!/bin/bash

function configureServer() {
  if [ ! -f .env ]; then
    env > .env
  fi
}

if [ "$1" = 'start' ]; then
  configureServer
  node dist/main.js
fi

exec "$@"
