import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsNotEmpty, IsString } from 'class-validator';

export class ExecuteBenchCommandsDto {
  @IsArray()
  @IsNotEmpty()
  @ApiProperty()
  commands: string[];

  @IsArray()
  @IsNotEmpty()
  @ApiProperty()
  options: string[];

  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  benchUid: string;
}
