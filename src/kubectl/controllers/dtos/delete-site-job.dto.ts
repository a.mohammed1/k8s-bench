import { ApiProperty } from '@nestjs/swagger';
import { IsFQDN, IsString } from 'class-validator';

export class DeleteSiteJobDto {
  @IsFQDN()
  @ApiProperty()
  siteName: string;

  @IsString()
  @ApiProperty()
  benchUid: string;
}
