import { Injectable } from '@nestjs/common';
import * as k8s from '@kubernetes/client-node';
import { AggregateRoot } from '@nestjs/cqrs';
import { ConfigService } from '@nestjs/config';

import { SiteIngressDeletedEvent } from '../../events/site-ingress-deleted/site-ingress-deleted.event';
import { ADMIN_PASSWORD, DELETE_SITE } from '../../../constants/app-constants';
import { CREATE_NEW_SITE } from '../../resources/create-site.k8s-job';
import { KubectlService } from '../kubectl/kubectl.service';
import { ERPNEXT_NAMESPACE } from '../../../constants/config-options';

@Injectable()
export class DeleteIngressAggregateService extends AggregateRoot {
  constructor(
    private readonly kubectl: KubectlService,
    private readonly config: ConfigService,
  ) {
    super();
  }

  async deleteSiteResources(siteName: string) {
    const namespace = this.config.get<string>(ERPNEXT_NAMESPACE);
    const coreV1Api = this.kubectl.k8sConfig.makeApiClient(k8s.CoreV1Api);
    const batchV1Api = this.kubectl.k8sConfig.makeApiClient(k8s.BatchV1Api);
    const networkingV1Api = this.kubectl.k8sConfig.makeApiClient(
      k8s.NetworkingV1Api,
    );

    try {
      await coreV1Api.deleteNamespacedSecret(
        `${siteName}-${ADMIN_PASSWORD}`,
        namespace,
      );
    } catch (error) {}

    try {
      await batchV1Api.deleteNamespacedJob(
        `${CREATE_NEW_SITE}-${siteName}`,
        namespace,
      );
    } catch (error) {}

    try {
      await batchV1Api.deleteNamespacedJob(
        `${DELETE_SITE}-${siteName}`,
        namespace,
      );
    } catch (error) {}

    try {
      await networkingV1Api.deleteNamespacedIngress(siteName, namespace);
      this.apply(new SiteIngressDeletedEvent(siteName));
    } catch (error) {}
  }
}
