import { KubeConfig } from '@kubernetes/client-node';
import { Injectable, OnModuleInit } from '@nestjs/common';
import * as k8s from '@kubernetes/client-node';
import { ConfigService } from '@nestjs/config';
import { NODE_ENV } from '../../../constants/config-options';

@Injectable()
export class KubectlService implements OnModuleInit {
  k8sConfig: KubeConfig = new k8s.KubeConfig();

  constructor(private readonly config: ConfigService) {}

  onModuleInit() {
    const nodeEnv = this.config.get<string>(NODE_ENV);
    if (nodeEnv === 'development') {
      this.k8sConfig.loadFromDefault();
    } else {
      this.k8sConfig.loadFromCluster();
    }
  }
}
