import { BadRequestException, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { AggregateRoot } from '@nestjs/cqrs';
import { from } from 'rxjs';
import * as k8s from '@kubernetes/client-node';

import {
  ERPNEXT_NAMESPACE,
  ERPNEXT_VERSION,
  IMAGE_PULL_SECRET_NAME,
  PVC_NAME,
  PY_IMAGE,
} from '../../../constants/config-options';
import { KubectlService } from '../kubectl/kubectl.service';
import { Request } from 'express';
import { ExecuteBenchCommandsDto } from '../../controllers/dtos/execute-bench-commands.dto';
import { executeBenchCommandJobJsonTemplate } from '../../resources/execute-bench-commands.k8s-job';
import { randomBytes } from 'crypto';

export const REQUIRED_PARAMETER_EXECPTION_MESSAGE =
  'Atleast one paramter either command or options is required';
@Injectable()
export class ExecuteBenchCommandAggregateService extends AggregateRoot {
  constructor(
    private readonly config: ConfigService,
    private readonly kubectl: KubectlService,
  ) {
    super();
  }

  async executeBenchCommandsJob(
    payload: ExecuteBenchCommandsDto,
    req: Request & { idToken: unknown },
  ) {
    if (payload.commands.length || payload.options.length) {
      const batchV1Api = this.kubectl.k8sConfig.makeApiClient(k8s.BatchV1Api);
      const namespace = this.config.get<string>(ERPNEXT_NAMESPACE);
      const pvcName = this.config.get<string>(PVC_NAME);
      const version = this.config.get<string>(ERPNEXT_VERSION);
      const jobName = randomBytes(16).toString('hex');
      const workerImage = this.config.get<string>(PY_IMAGE);
      const imgPullSecret = this.config.get<string>(IMAGE_PULL_SECRET_NAME);
      const jobBody = executeBenchCommandJobJsonTemplate(
        jobName,
        namespace,
        version,
        pvcName,
        payload.commands,
        payload.options,
        workerImage,
        imgPullSecret,
      );
      return await from(batchV1Api.createNamespacedJob(namespace, jobBody))
        .toPromise()
        .catch(err => {
          Logger.log(err);
        });
    }
    throw new BadRequestException(REQUIRED_PARAMETER_EXECPTION_MESSAGE);
  }
}
