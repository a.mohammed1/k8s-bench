import { Test, TestingModule } from '@nestjs/testing';
import { ConfigService } from '@nestjs/config';
import { ExecuteBenchCommandAggregateService } from './execute-bench-commands-aggregate.service';
import { KubectlService } from '../kubectl/kubectl.service';

describe('ExecuteBenchCommandAggregateService', () => {
  let service: ExecuteBenchCommandAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ExecuteBenchCommandAggregateService,
        { provide: ConfigService, useValue: {} },
        { provide: KubectlService, useValue: {} },
      ],
    }).compile();

    service = module.get<ExecuteBenchCommandAggregateService>(
      ExecuteBenchCommandAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
