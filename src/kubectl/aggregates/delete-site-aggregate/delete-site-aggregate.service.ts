import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { AggregateRoot } from '@nestjs/cqrs';
import * as k8s from '@kubernetes/client-node';

import { ERPNEXT } from '../../../constants/app-constants';
import {
  ERPNEXT_NAMESPACE,
  PVC_NAME,
  ERPNEXT_VERSION,
  ERPNEXT_HELM_RELEASE,
  BENCH_UID,
  MARIADB_SECRET_NAME,
  DELETE_SITE_CONFIG_MAP,
  IMAGE_PULL_SECRET_NAME,
  PY_IMAGE,
} from '../../../constants/config-options';
import { generateDeleteSiteJobJsonTemplate } from '../../resources/delete-site.k8s-job';
import { DeleteSiteJobDto } from '../../controllers/dtos/delete-site-job.dto';
import { KubectlService } from '../kubectl/kubectl.service';

@Injectable()
export class DeleteSiteAggregateService extends AggregateRoot {
  constructor(
    private readonly config: ConfigService,
    private readonly kubectl: KubectlService,
  ) {
    super();
  }

  async deleteSiteJob(payload: DeleteSiteJobDto) {
    const requestedBenchUid = payload.benchUid;
    const benchUid = this.config.get<string>(BENCH_UID);

    if (benchUid === requestedBenchUid) {
      const batchV1Api = this.kubectl.k8sConfig.makeApiClient(k8s.BatchV1Api);
      const namespace = this.config.get<string>(ERPNEXT_NAMESPACE);
      const pvcName = this.config.get<string>(PVC_NAME);
      const version = this.config.get<string>(ERPNEXT_VERSION);
      const mariadbSecretName = this.config.get<string>(MARIADB_SECRET_NAME);
      const deleteSiteCMName = this.config.get<string>(DELETE_SITE_CONFIG_MAP);
      const workerImage = this.config.get<string>(PY_IMAGE);
      const imagePullSecretName = this.config.get<string>(
        IMAGE_PULL_SECRET_NAME,
      );
      const svcName = `${this.config.get<string>(
        ERPNEXT_HELM_RELEASE,
      )}-${ERPNEXT.toLowerCase()}`;

      const jobBody = generateDeleteSiteJobJsonTemplate(
        payload.siteName,
        namespace,
        version,
        pvcName,
        svcName,
        benchUid,
        mariadbSecretName,
        deleteSiteCMName,
        workerImage,
        imagePullSecretName,
      );

      const response = await batchV1Api.createNamespacedJob(namespace, jobBody);
      return response;
    }

    return {};
  }
}
