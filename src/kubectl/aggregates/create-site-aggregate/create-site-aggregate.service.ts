import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { AggregateRoot } from '@nestjs/cqrs';
import { randomBytes } from 'crypto';
import { from, of, throwError } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import * as k8s from '@kubernetes/client-node';

import { generateSiteJobJsonTemplate } from '../../resources/create-site.k8s-job';
import {
  ERPNEXT_NAMESPACE,
  ERPNEXT_HELM_RELEASE,
  ERPNEXT_VERSION,
  PVC_NAME,
  BENCH_UID,
  MARIADB_SECRET_NAME,
  WIZARD_SCRIPT_CONFIG_MAP,
  IMAGE_PULL_SECRET_NAME,
  PY_IMAGE,
} from '../../../constants/config-options';
import { ERPNEXT } from '../../../constants/app-constants';
import { generateSecretTemplate } from '../../resources/create-secret.k8s-secret';
import { CreateNewSiteJobDto } from '../../controllers/dtos/create-new-site-job.dto';
import { KubectlService } from '../kubectl/kubectl.service';
import { Request } from 'express';

@Injectable()
export class CreateSiteAggregateService extends AggregateRoot {
  constructor(
    private readonly config: ConfigService,
    private readonly kubectl: KubectlService,
  ) {
    super();
  }

  async createSiteJob(
    payload: CreateNewSiteJobDto,
    req: Request & { idToken: unknown },
  ) {
    const requestedBenchUid = payload.benchUid;
    const benchUid = this.config.get<string>(BENCH_UID);

    if (benchUid === requestedBenchUid) {
      const batchV1Api = this.kubectl.k8sConfig.makeApiClient(k8s.BatchV1Api);
      const coreV1Api = this.kubectl.k8sConfig.makeApiClient(k8s.CoreV1Api);

      const namespace = this.config.get<string>(ERPNEXT_NAMESPACE);
      const pvcName = this.config.get<string>(PVC_NAME);
      const version = this.config.get<string>(ERPNEXT_VERSION);
      const mariadbSecret = this.config.get<string>(MARIADB_SECRET_NAME);
      const wizardScriptCM = this.config.get<string>(WIZARD_SCRIPT_CONFIG_MAP);
      const imgPullSecretName = this.config.get<string>(IMAGE_PULL_SECRET_NAME);
      const svcName = `${this.config.get<string>(
        ERPNEXT_HELM_RELEASE,
      )}-${ERPNEXT.toLowerCase()}`;

      const adminPasswordBase64 = Buffer.from(
        randomBytes(32).toString('hex'),
      ).toString('base64');

      const secretBody = generateSecretTemplate(
        adminPasswordBase64,
        payload.siteName,
      );
      const workerImage = this.config.get<string>(PY_IMAGE);
      const jobBody = generateSiteJobJsonTemplate(
        payload,
        namespace,
        version,
        pvcName,
        svcName,
        payload.apps,
        benchUid,
        mariadbSecret,
        wizardScriptCM,
        workerImage,
        imgPullSecretName,
      );

      return await from(coreV1Api.createNamespacedSecret(namespace, secretBody))
        .pipe(
          switchMap(() => {
            return from(batchV1Api.createNamespacedJob(namespace, jobBody));
          }),
          catchError(error => {
            Logger.error(error, error.toString(), this.constructor.name);
            return throwError(() => error);
          }),
        )
        .toPromise();
    }

    return of({});
  }
}
