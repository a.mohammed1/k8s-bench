import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { DeleteIngressAggregateService } from '../../aggregates/delete-ingress-aggregate/delete-ingress-aggregate.service';
import { DeleteSiteResourcesCommand } from './delete-site-resources.command';

@CommandHandler(DeleteSiteResourcesCommand)
export class DeleteSiteIngressHandler implements ICommandHandler {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: DeleteIngressAggregateService,
  ) {}

  async execute(command: DeleteSiteResourcesCommand) {
    const { siteName } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.deleteSiteResources(siteName);
    aggregate.commit();
  }
}
