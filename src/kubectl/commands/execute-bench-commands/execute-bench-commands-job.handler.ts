import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { ExecuteBenchCommandsCommand } from './execute-bench-commands-job.command';
import { ExecuteBenchCommandAggregateService } from '../../aggregates/execute-bench-commands-aggregate/execute-bench-commands-aggregate.service';

@CommandHandler(ExecuteBenchCommandsCommand)
export class ExecuteBenchCommandsJobHandler
  implements ICommandHandler<ExecuteBenchCommandsCommand>
{
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: ExecuteBenchCommandAggregateService,
  ) {}

  async execute(command: ExecuteBenchCommandsCommand) {
    const { payload, request } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.executeBenchCommandsJob(payload, request);
    aggregate.commit();
  }
}
