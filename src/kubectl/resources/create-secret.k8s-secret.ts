import { V1Secret } from '@kubernetes/client-node';

export const generateSecretTemplate = (
  password: string,
  siteName: string,
): V1Secret => {
  return {
    data: { password },
    metadata: { name: `${siteName}-admin-password` },
    type: 'Opaque',
  };
};
