import { V1Job } from '@kubernetes/client-node';
import { SITES_DIR } from '../../constants/app-constants';

export const EXECUTE_BENCH_COMMANDS = 'execute-bench-commands';
export const RUN_WIZARD = 'run-wizard';

export const executeBenchCommandJobJsonTemplate = (
  jobName: string,
  namespace: string,
  version: string,
  pvc: string,
  commands: string[],
  options: string[],
  workerImage: string,
  imagePullSecretName?: string,
): V1Job => {
  const job: V1Job = {
    metadata: {
      name: jobName,
      namespace,
    },
    spec: {
      backoffLimit: 0,
      template: {
        spec: {
          imagePullSecrets: imagePullSecretName
            ? [{ name: imagePullSecretName }]
            : [],
          securityContext: {
            supplementalGroups: [1000],
          },
          containers: [
            {
              name: RUN_WIZARD,
              image: `${workerImage}:${version}`,
              command: commands.length ? ['bench', ...commands] : ['bench'],
              args: options.length ? options : [],
              imagePullPolicy: 'IfNotPresent',
              volumeMounts: [
                {
                  name: SITES_DIR,
                  mountPath: '/home/frappe/frappe-bench/sites',
                },
              ],
            },
          ],
          restartPolicy: 'Never',
          volumes: [
            {
              name: SITES_DIR,
              persistentVolumeClaim: {
                claimName: pvc,
                readOnly: false,
              },
            },
          ],
        },
      },
    },
  };

  return job;
};
