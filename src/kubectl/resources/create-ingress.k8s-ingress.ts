import { V1Ingress } from '@kubernetes/client-node';
import { INGRESS_PATH_TYPE } from '../../constants/app-constants';
import { IngressConfig } from '../providers/ingress-config.provider';

/**
 * GenerateIngressTemplate
 * @param siteName FQDN of site
 * @param serviceName kubernetes service for ingress
 * @param ingressConfig annotations config
 * @param wildCardDomain '*.example.com'
 * @param wildCardTlsSecretName name of kubernetes secret for wildcard certificate
 */
export const generateIngressTemplate = (
  siteName: string,
  serviceName: string,
  ingressConfig: IngressConfig,
  wildCardDomain?: string,
  wildCardTlsSecretName?: string,
): V1Ingress => {
  const ingress: V1Ingress = {
    metadata: {
      name: `${siteName}`,
      labels: ingressConfig.labels,
      annotations: ingressConfig.annotations,
    },
    spec: {
      rules: [
        {
          host: siteName,
          http: {
            paths: [
              {
                backend: {
                  service: {
                    name: serviceName,
                    port: { number: 8080 },
                  },
                },
                path: '/',
                pathType: INGRESS_PATH_TYPE,
              },
            ],
          },
        },
      ],
      tls: [],
    },
  };

  if (wildCardDomain && wildCardTlsSecretName) {
    ingress.spec.tls.push({
      hosts: [wildCardDomain],
      secretName: wildCardTlsSecretName,
    });
  } else {
    ingress.spec.tls.push({
      hosts: [siteName],
      secretName: `${siteName}-tls`,
    });
  }

  return ingress;
};
