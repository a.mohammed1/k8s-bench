import { V1Job } from '@kubernetes/client-node';
import {
  SITES_DIR,
  WIZARD_SCRIPT,
  ADMIN_PASSWORD,
} from '../../constants/app-constants';
import { CreateNewSiteJobDto } from '../controllers/dtos/create-new-site-job.dto';

export const CREATE_NEW_SITE = 'create-new-site';
export const RUN_WIZARD = 'run-wizard';

export const generateSiteJobJsonTemplate = (
  wizard: CreateNewSiteJobDto,
  namespace: string,
  version: string,
  pvc: string,
  svcName: string,
  apps: string[],
  benchUid: string,
  mariadbRootPasswordSecretName: string,
  wizardScriptConfigMapName: string,
  workerImage: string,
  imagePullSecretName?: string,
): V1Job => {
  const annotations = {
    benchService: svcName,
    wildcardDomain: undefined,
    wildcardTlsSecretName: undefined,
    benchUid,
  };

  if (wizard.wildcardDomain) {
    annotations.wildcardDomain = wizard.wildcardDomain;
  }

  if (wizard.wildcardTlsSecretName) {
    annotations.wildcardTlsSecretName = wizard.wildcardTlsSecretName;
  }

  const job: V1Job = {
    metadata: {
      name: `${CREATE_NEW_SITE}-${wizard.siteName}`,
      namespace,
      annotations,
    },
    spec: {
      backoffLimit: 0,
      template: {
        spec: {
          imagePullSecrets: imagePullSecretName
            ? [{ name: imagePullSecretName }]
            : [],
          securityContext: {
            supplementalGroups: [1000],
          },
          initContainers: [
            {
              name: CREATE_NEW_SITE,
              image: `${workerImage}:${version}`,
              args: ['new'],
              imagePullPolicy: 'IfNotPresent',
              volumeMounts: [
                {
                  name: SITES_DIR,
                  mountPath: '/home/frappe/frappe-bench/sites',
                },
              ],
              env: [
                { name: 'SITE_NAME', value: wizard.siteName },
                { name: 'DB_ROOT_USER', value: 'root' },
                {
                  name: 'MYSQL_ROOT_PASSWORD',
                  valueFrom: {
                    secretKeyRef: {
                      key: 'password',
                      name: mariadbRootPasswordSecretName,
                    },
                  },
                },
                {
                  name: 'ADMIN_PASSWORD',
                  valueFrom: {
                    secretKeyRef: {
                      key: 'password',
                      name: `${wizard.siteName}-${ADMIN_PASSWORD}`,
                    },
                  },
                },
              ],
            },
          ],
          containers: [
            {
              name: RUN_WIZARD,
              image: `${workerImage}:${version}`,
              command: ['/home/frappe/frappe-bench/env/bin/python'],
              args: ['/opt/frappe/wizard.py'],
              imagePullPolicy: 'IfNotPresent',
              volumeMounts: [
                {
                  name: SITES_DIR,
                  mountPath: '/home/frappe/frappe-bench/sites',
                },
                {
                  name: WIZARD_SCRIPT,
                  mountPath: '/opt/frappe',
                },
              ],
              env: [
                { name: 'SITE_NAME', value: wizard.siteName },
                { name: 'USER_LANGUAGE', value: wizard.language },
                { name: 'COUNTRY', value: wizard.country },
                { name: 'TIMEZONE', value: wizard.timezone },
                { name: 'FULL_NAME', value: wizard.fullName },
                { name: 'EMAIL', value: wizard.email },
                { name: 'PASSWORD', value: wizard.password },
              ],
            },
          ],
          restartPolicy: 'Never',
          volumes: [
            {
              name: SITES_DIR,
              persistentVolumeClaim: {
                claimName: pvc,
                readOnly: false,
              },
            },
            {
              name: WIZARD_SCRIPT,
              configMap: {
                name: wizardScriptConfigMapName,
              },
            },
          ],
        },
      },
    },
  };

  job.spec.template.spec.initContainers.map(container => {
    if (apps?.length > 0) {
      container.env.push({
        name: 'INSTALL_APPS',
        value: apps.join(','),
      });
    }
  });
  return job;
};
