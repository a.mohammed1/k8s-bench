import { IQuery } from '@nestjs/cqrs';

export class GetIngressQuery implements IQuery {
  constructor(public readonly ingressName: string) {}
}
