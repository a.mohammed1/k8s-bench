import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { WatchJobsService } from './kubectl/watch/watch-jobs/watch-jobs.service';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const watchJobs = app.get(WatchJobsService);
  app.init().then(() => watchJobs.start());
}
bootstrap();
