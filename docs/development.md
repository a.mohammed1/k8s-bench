### Install K3d Cluster

```shell
k3d cluster create devcluster \
  --api-port 127.0.0.1:6443 \
  -p 80:80@loadbalancer \
  -p 443:443@loadbalancer \
  --k3s-server-arg "--no-deploy=traefik"
```

### Add Helm Repo

```shell
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo add frappe https://helm.erpnext.com
helm repo update
```

### Install MariaDB

```shell
kubectl create ns mariadb
helm install mariadb -n mariadb bitnami/mariadb -f ./docs/example/mariadb-values.yaml
```

### Install NFS

```shell
kubectl create ns nfs
kubectl create -f ./docs/example/nfs-server-provisioner/statefulset.dev.yaml
kubectl create -f ./docs/example/nfs-server-provisioner/rbac.yaml
kubectl create -f ./docs/example/nfs-server-provisioner/class.yaml
```

### Install Redis

```shell
kubectl create ns redis
helm install redis -n redis bitnami/redis \
  --set auth.enabled=false \
  --set auth.sentinal=false \
  --set architecture=standalone \
  --set master.persistence.enabled=false
```

### Install ERPNext

```shell
kubectl create ns erpnext
cp ./docs/example/erpnext-example-values.yaml ./docs/example/erpnext-values.yaml
editor ./docs/example/erpnext-values.yaml
helm install erpnext-dev -n erpnext frappe/erpnext -f ./docs/example/erpnext-values.yaml
```

Note: Create `imagePullSecrets` if necessary and set it in `values.yaml`

### Create resources

```
helm template dev -n erpnext deploy/k8s-bench --set isProdMode=false | kubectl -n erpnext apply -f -
```

### Install ERPNext (upgrade bench)


```shell
cp ./docs/example/erpnext-example-upgrade-values.yaml ./docs/example/erpnext-upgrade-values.yaml
editor ./docs/example/erpnext-values.yaml
helm install erpnext-upgrade -n erpnext frappe/erpnext -f ./docs/example/erpnext-upgrade-values.yaml
```

Note:

- Create `imagePullSecrets` if necessary and set it in `values.yaml`
- Make sure the versions are newer than erpnext-dev release

### Create resources for upgrade release

```
helm template upgrade -n erpnext deploy/k8s-bench --set isProdMode=false | kubectl -n erpnext apply -f -
```

### Install Ingress Controller (Optional)

Follow this step to try out created sites locally on port 80 and 443.

```shell
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.0.0/deploy/static/provider/cloud/deploy.yaml
```

### Start Backing Services (MQTT)

```shell
docker-compose -f services.yml up -d
```

### Install node_modules

```shell
yarn
```

### Start k8s-bench

```shell
cp env-example ./.env
yarn start
```

Note: change env file as per need

### Start k8s-bench for upgrade release

```shell
cp env-upgrade ./upgrade.env
ENV_PATH=upgrade.env yarn start
```

Note: change env file as per need

### Start k8s-bench job watcher

```shell
cp env-example ./events.env
sed -i -e 's|#||g' events.env
sed -i -e 's|APP_PORT=7000|APP_PORT=7070|g' events.env
ENV_PATH=events.env yarn start
```

Note:

- Change env file as per need
- We need only one watcher

### HTTP Requests

Create New Site Request

```shell
curl -X 'POST' \
  'http://localhost:7000/kube/v1/new_site_request' \
  -H 'accept: */*' \
  -H 'Authorization: Basic NDA1MTVlYWYtOWEyNC00NmRiLTljODktNjY1Y2U4Yjg0MWIwOjA3MDU0NzBiOTRlMDM5MzExNGJlZDJiYTgxOGU4MThiNGUyYmM4NmNlOTM0ODhjNzM4ZWUzZmY1NzMxMTliOWY=' \
  -H 'Content-Type: application/json' \
  -d '{
  "siteName": "test0.cluster.localhost",
  "benchUid": "40515eaf-9a24-46db-9c89-665ce8b841b0",
  "apps": [],
  "language": "en",
  "country": "India",
  "timezone": "Asia/Kolkata",
  "fullName": "Castlecraft",
  "email": "support@castlecraft.in",
  "password": "123",
  "wildcardDomain": "cluster.localhost",
  "wildcardTlsSecretName": "cluster-localhost-tls",
  "owner": "support@castlecraft.in"
}'
```

Create Site Ingress

```shell
curl -X 'POST' \
  'http://localhost:7000/kube/v1/create_site_ingress' \
  -H 'accept: */*' \
  -H 'Authorization: Basic NDA1MTVlYWYtOWEyNC00NmRiLTljODktNjY1Y2U4Yjg0MWIwOjA3MDU0NzBiOTRlMDM5MzExNGJlZDJiYTgxOGU4MThiNGUyYmM4NmNlOTM0ODhjNzM4ZWUzZmY1NzMxMTliOWY=' \
  -H 'Content-Type: application/json' \
  -d '{
  "svcName": "erpnext-dev",
  "jobName": "create-new-site-test0.cluster.localhost",
  "namespace": "erpnext",
  "wildcardDomain": "cluster.localhost",
  "wildcardTlsSecretName": "cluster-localhost-tls"
}'
```

Upgrade Site

```shell
curl -X 'POST' \
  'http://localhost:7007/kube/v1/upgrade_site' \
  -H 'accept: */*' \
  -H 'Authorization: Basic ZWU5NWFlN2UtYWJmNy00N2M1LTg0NzAtNTY5MDE1MmYyODhjOjI5NjQyYjljODUzMzk3NmEwMjFlMTk1MjRmZWIyZjIwMTM5YzE4MjYwYjQzNmNlZDIyNTE0ZmYxNzM1NTdlZDY=' \
  -H 'Content-Type: application/json' \
  -d '{
  "siteName": "test0.cluster.localhost",
  "basePyImage": "frappe/erpnext-worker:v13.9.2",
  "basePvc": "erpnext-dev"
}'
```

Note: Upgrade site request is made on new bench and not on the same bench.
