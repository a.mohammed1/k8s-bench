# Default values for k8s-bench.
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.

isProdMode: true
isWatcherInstalled: false
replicaCount: 1

image:
  repository: registry.gitlab.com/castlecraft/k8s-bench
  tag: latest
  pullPolicy: IfNotPresent

imagePullSecrets: []
nameOverride: ""
fullnameOverride: ""

serviceAccount:
  # Specifies whether a service account should be created
  create: true
  # Annotations to add to the service account
  annotations: {}
  # The name of the service account to use.
  # If not set and create is true, a name is generated using the fullname template
  name:

podSecurityContext: {}
  # fsGroup: 2000

securityContext: {}
  # capabilities:
  #   drop:
  #   - ALL
  # readOnlyRootFilesystem: true
  # runAsNonRoot: true
  # runAsUser: 1000

service:
  type: ClusterIP
  port: 7000

ingress:
  enabled: false
  annotations:
    kubernetes.io/ingress.class: nginx
    # kubernetes.io/tls-acme: "true"
  hosts:
    - host: k8s-bench.localhost
      paths:
        - /
  tls: []
  #  - secretName: chart-example-tls
  #    hosts:
  #      - chart-example.local

resources: {}
  # We usually recommend not to specify default resources and to leave this as a conscious
  # choice for the user. This also increases chances charts run on environments with little
  # resources, such as Minikube. If you do want to specify resources, uncomment the following
  # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
  # limits:
  #   cpu: 100m
  #   memory: 128Mi
  # requests:
  #   cpu: 100m
  #   memory: 128Mi

nodeSelector: {}

tolerations: []

affinity: {}

configMaps:
  deleteSitePy: |-
    import frappe
    import os
    import shutil


    def drop_site(site, root_login='root', root_password=None):
        "Remove site from database and filesystem"
        from frappe.database import drop_user_and_database

        frappe.init(site=site)
        frappe.connect()

        drop_user_and_database(frappe.conf.db_name, root_login, root_password)

        try:
            shutil.rmtree(site)
        except OSError as e:
            print("Error: {site} : {error}".format(site=site, error=e.strerror))


    def main():
        site_name = os.environ.get("SITE_NAME")
        root_user = os.environ.get("DB_ROOT_USER", "root")
        root_password = os.environ.get("DB_ROOT_PASSWORD")

        if not site_name or not root_password:
            if not site_name: print("SITE_NAME environment variable required to be set")
            if not root_password: print("DB_ROOT_PASSWORD environment variable required to be set")
            exit(1)

        drop_site(
            site=site_name,
            root_login=root_user,
            root_password=root_password,
        )


    if __name__ == "__main__":
        main()

  upgradeSitePy: |-
    import frappe
    import os
    import shutil
    import glob
    import subprocess
    import json

    from distutils.dir_util import copy_tree
    from frappe.installer import update_site_config
    from frappe.migrate import migrate

    FROM_BENCH_PATH = 'FROM_BENCH_PATH'
    SITE_NAME = 'SITE_NAME'
    MAINTENANCE_MODE = 'maintenance_mode'
    PAUSE_SCHEDULER = 'pause_scheduler'
    SITE_CONFIG_FILE = 'site_config.json'
    COMMON_SITE_CONFIG_FILE = 'common_site_config.json'

    def main():
        env = get_env()
        from_site_config_path = os.path.join(
            env.get(FROM_BENCH_PATH),
            env.get(SITE_NAME),
            SITE_CONFIG_FILE,
        )

        set_maintenance_mode(from_site_config_path)

        copy_site_stub_from_bench(
            env.get(FROM_BENCH_PATH),
            env.get(SITE_NAME),
        )

        try:
            migrate_site(env.get(SITE_NAME))

            # on successful migration, move skipped files
            copy_user_files(env.get(FROM_BENCH_PATH), env.get(SITE_NAME))

            # delete site_name from_bench_path
            delete_site_dir(os.path.join(
                env.get(FROM_BENCH_PATH),
                env.get(SITE_NAME),
            ))

            unset_maintenance_mode(
                os.path.join('.', env.get(SITE_NAME), SITE_CONFIG_FILE)
            )

            frappe.destroy()
        except Exception as exc:

            # if failed migration, retore from previous backup
            restore_previous_db(env)

            # delete site_name directory from new bench
            delete_site_dir(os.path.join(
                '.',
                env.get(SITE_NAME),
            ))

            # log error
            print(repr(exc))
            unset_maintenance_mode(
                os.path.join(
                    env.get(FROM_BENCH_PATH),
                    env.get(SITE_NAME),
                    SITE_CONFIG_FILE,
                )
            )
            frappe.destroy()
            exit(1)


    def get_env():
        env = {
            f'{FROM_BENCH_PATH}': os.environ.get(FROM_BENCH_PATH),
            f'{SITE_NAME}': os.environ.get(SITE_NAME),
        }

        if not env.get(FROM_BENCH_PATH):
            print(f"environment variable {FROM_BENCH_PATH} not set")
            exit(1)

        if not env.get(SITE_NAME):
            print(f"environment variable {SITE_NAME} not set")
            exit(1)

        return env


    def set_maintenance_mode(site_config_path):
        print(f'Set Maintenance Mode for {site_config_path}')
        update_site_config(
            key=MAINTENANCE_MODE,
            value=1,
            site_config_path=site_config_path,
        )
        update_site_config(
            key=PAUSE_SCHEDULER,
            value=1,
            site_config_path=site_config_path,
        )


    def unset_maintenance_mode(site_config_path):
        print(f'Unset Maintenance Mode for {site_config_path}')
        update_site_config(
            key=MAINTENANCE_MODE,
            value=0,
            site_config_path=site_config_path,
        )
        update_site_config(
            key=PAUSE_SCHEDULER,
            value=0,
            site_config_path=site_config_path,
        )


    def copy_site_stub_from_bench(sites_path, site_name):
        print(f'Copy site stub from {sites_path} for {site_name}')
        if not os.path.exists(os.path.join('.', site_name)):
            os.makedirs(os.path.join('.', site_name))

        if not os.path.exists(os.path.join('.', site_name, SITE_CONFIG_FILE)):
            shutil.copy2(
                os.path.join(sites_path, site_name, SITE_CONFIG_FILE),
                os.path.join('.', site_name, SITE_CONFIG_FILE),
            )


    def migrate_site(site):
        print('Migrating', site)
        frappe.init(site=site)
        frappe.connect()
        migrate()


    def copy_user_files(from_bench_path, site_name):
        try:
            print("Copying private and public directories for site")
            copy_tree(
                os.path.join(from_bench_path, site_name, 'private'),
                os.path.join('.', site_name, 'private')
            )
            copy_tree(
                os.path.join(from_bench_path, site_name, 'public'),
                os.path.join('.', site_name, 'public')
            )
        except Exception as exc:
            print(repr(exc))
            exit(1)


    def delete_site_dir(site_dir):
        try:
            print(f'Deleting {site_dir}')
            shutil.rmtree(site_dir, ignore_errors=True)
        except Exception as exc:
            print(repr(exc))
            exit(1)


    def restore_previous_db(env):
        print("Restoring old DB")
        versions = None
        latest_backup_gz = max(
            glob.iglob(
                os.path.join(
                    env.get(FROM_BENCH_PATH),
                    env.get(SITE_NAME),
                    'private',
                    'backups',
                    '*-database.sql.gz',
                )
            ),
            key=os.path.getctime,
        )
        latest_backup = latest_backup_gz.replace(".gz", "")

        config = get_config()
        site_config = get_site_config(env.get(SITE_NAME))

        db_host = site_config.get('db_host', config.get('db_host'))
        db_port = site_config.get('db_port', config.get('db_port', 3306))
        db_name = site_config.get('db_name')
        db_password = site_config.get('db_password')

        command = ["gunzip", "-c", latest_backup_gz]

        with open(latest_backup, "w") as db_file:
            print('Extract Database GZip for site {}'.format(env.get(SITE_NAME)))
            run_command(command, stdout=db_file)

        mysql_command = ["mysql", f"-u{db_name}", f"-h{db_host}", f"-p{db_password}", f"-P{db_port}"]

        # drop db if exists for clean restore
        drop_database = mysql_command + ["-e",  f"DROP DATABASE IF EXISTS `{db_name}`;"]
        run_command(drop_database)

        # create db
        create_database = mysql_command + ["-e", f"CREATE DATABASE IF NOT EXISTS `{db_name}`;"]
        run_command(create_database)

        print('Restoring MariaDB')
        with open(latest_backup, 'r') as db_file:
            run_command(mysql_command + [f"{db_name}"], stdin=db_file)

        if os.path.isfile(latest_backup):
            os.remove(latest_backup)


    def run_command(command, stdout=None, stdin=None, stderr=None):
        stdout = stdout or subprocess.PIPE
        stderr = stderr or subprocess.PIPE
        stdin = stdin or subprocess.PIPE
        process = subprocess.Popen(command, stdout=stdout, stdin=stdin, stderr=stderr)
        out, error = process.communicate()
        if process.returncode:
            print("Something went wrong:")
            print(f"return code: {process.returncode}")
            print(f"stdout:\n{out}")
            print(f"\nstderr:\n{error}")
            exit(process.returncode)


    def get_config():
        config = None
        try:
            with open(COMMON_SITE_CONFIG_FILE) as config_file:
                config = json.load(config_file)
        except FileNotFoundError as exception:
            print(exception)
            exit(1)
        except Exception:
            print(COMMON_SITE_CONFIG_FILE + " is not valid")
            exit(1)
        return config


    def get_site_config(site_name):
        site_config = None
        with open(f'{site_name}/{SITE_CONFIG_FILE}') as site_config_file:
            site_config = json.load(site_config_file)
        return site_config


    if __name__ == "__main__":
        main()

  wizardPy: |-
    import os
    import frappe
    import erpnext
    import frappe.utils.user
    from erpnext.setup.setup_wizard.setup_wizard import setup_complete
    from frappe.utils import get_sites


    def main():
        # run setup wizard
        site = os.environ.get('SITE_NAME')
        if site not in get_sites():
            print("Site {0} does not exist on the current bench".format(site))
            return

        email = os.environ.get('EMAIL')
        if not email:
            print("Set email, exiting...")
            return

        password = os.environ.get('PASSWORD')
        if not password:
            print("Set password, exiting...")
            return

        frappe.init(site=site)
        frappe.connect()

        # Add first system manager
        frappe.utils.user.add_system_manager(
            email,
            os.environ.get('FULL_NAME'),
            None,
            False,
            password,
        )
        frappe.db.commit()

        # Save System Settings (needed for ERPNext v12.13.0)
        system_settings = frappe.get_doc("System Settings")
        system_settings.country = os.environ.get('COUNTRY', 'India')
        system_settings.language = os.environ.get('USER_LANGUAGE', 'en')
        system_settings.time_zone = os.environ.get('TIMEZONE', 'Asia/Kolkata')
        system_settings.save(ignore_permissions=True)
        frappe.db.commit()


    if __name__ == "__main__":
        main()

  ingressConfigJson: |-
    {
        "labels": {
            "app.kubernetes.io/instance": "k8s-bench"
        },
        "annotations": {
            "bench-manager": "k8s-bench"
        }
    }

envVars:
    nodeEnv: production
    benchApiSecret: 0705470b94e0393114bed2ba818e818b4e2bc86ce93488c738ee3ff573119b9f
    mariadbRootPassword: admin
    erpnextVersion: v13.9.2
    erpnextNamespace: erpnext
    pvc: erpnext-v13
    helmReleaseName: erpnext-v13
    benchUid: 40515eaf-9a24-46db-9c89-665ce8b841b0
    appPort: 7000
    pyImage: frappe/erpnext-worker
    nginxImage: frappe/erpnext-nginx
    mariadbSecretName: dev-k8s-bench-mariadb-root-password
    deleteSiteCMName: dev-k8s-bench-delete-site
    wizardCMName: dev-k8s-bench-wizard-script
    upgradeSiteCMName: dev-k8s-bench-upgrade-site
    imgPullSecretName: ""
    eventsHost: "host.k3d.internal"
    eventsPort: 1883
    eventsPassword: "changeit"
    eventsProto: "mqtt"
    eventsUser: "admin"
    eventsAppPort: 7070
